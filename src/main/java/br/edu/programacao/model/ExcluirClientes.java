package br.edu.programacao.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExcluirClientes {

	public void excluirClientes(String id) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clientes","postgres","postgres");
		PreparedStatement ps = conn.prepareStatement("delete from clientes where id=?");
		ps.setInt(1, Integer.parseInt(id));
		int res = ps.executeUpdate();
		System.out.println(res+" registro foi excluído");
		ps.close();
		conn.close();
	}
	
	public static void main(String[] args) throws SQLException {
		ExcluirClientes ec = new ExcluirClientes();
		ec.excluirClientes("1");
	}
	
}
