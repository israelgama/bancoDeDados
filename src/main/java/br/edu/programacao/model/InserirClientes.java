package br.edu.programacao.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InserirClientes {

	public void inserirCliente(String... cliente) throws SQLException {

		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clientes","postgres","postgres");
		if (conn != null) {
			PreparedStatement ps = conn.prepareStatement("insert into clientes values(?,?,?,?,?)");
			ps.setInt(1,Integer.parseInt(cliente[0]));
			ps.setString(2,cliente[1]);
			ps.setString(3,cliente[2]);
			ps.setString(4,cliente[3]);
			ps.setString(5,cliente[4]);
			
			int res = ps.executeUpdate();
			System.out.println(res +" usuário foi inserido!!");
			ps.close();
			conn.close();
		}
		
	}
	
	public static void main(String[] args) throws SQLException {
		InserirClientes ic = new InserirClientes();
		ic.inserirCliente(new String[]{"4","Luciano","Rua 6 Conj.1","Goiania","GO"});
	}
		
}
