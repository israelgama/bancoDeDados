package br.edu.programacao.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AtualizarClientes {

	
	public void atualizarClientes(String id, String endereco) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clientes","postgres","postgres");
		PreparedStatement ps = conn.prepareStatement("update clientes set endereco=? where id=?");
		ps.setString(1, endereco);
		ps.setInt(2, Integer.parseInt(id));
		int res = ps.executeUpdate();
		System.out.println(res+" registro foi atualizado!!");
		ps.close();
		conn.close();
	}
	
	public static void main(String[] args) throws SQLException {
		AtualizarClientes ac = new AtualizarClientes();
		ac.atualizarClientes("2", "Rua das tempestades");
	}
	
}
