package br.edu.programacao.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LerClientesStatement {

	/*
	 * Exemplo usando Statement
	 */
	public void LerClientes() throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/clientes","postgres","postgres");
		Statement stm = conn.createStatement();
		ResultSet rs = stm.executeQuery("Select * from clientes");
		while (rs.next()) {
			System.out.println("Código: "+rs.getInt(1));
			System.out.println("Nome: "+rs.getString(2));
			System.out.println("Endereço: "+rs.getString(3));
			System.out.println("Cidade: "+rs.getString(4));
			System.out.println("Uf: "+rs.getString(5));
			System.out.println("");			
		}
		rs.close();
		conn.close();
	}
	
	public static void main(String[] args) throws SQLException {
		LerClientesStatement lcs = new LerClientesStatement();
		lcs.LerClientes();
	}
	
}

